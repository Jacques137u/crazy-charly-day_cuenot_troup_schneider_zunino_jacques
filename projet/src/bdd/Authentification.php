<?php

namespace projet\bdd;

use projet\modeles\tables\Compte as Compte;

class Authentification{

/**
  * Méthode qui permet à un utilisateur de se connecter
  * @param String $username
  * @param String $password
  * @return boolean retourne si la connexion a fonctionné
 **/
  public static function connexion($username,$password){
    try{
        $compte = Compte::where('username','=',$username)->first();
          if(password_verify($password,$compte['password'])){
              $_SESSION['user']=$compte;
              return true;
          }
          return false;

    }catch (Exception $e) {
      return false;
    }
  }

/**
  * Méthode qui permet de s'inscrire sur le site
  * @param String $username
  * @param String $password
  * @return boolean retourne si l'inscription a fonctionné
 **/
  public static function inscription($username,$password){
    try{
          $compte = Compte::where('username','=',$username)->first();
          if($compte!=null){
            return false;
          }else{

            //Creation du COMPTE

            $compte = new Compte();
            $hash = password_hash($password,PASSWORD_BCRYPT);
            $type = htmlentities($_POST['registration_typecompte']);
            $compte->mail = htmlentities($_POST['registration_mail']);
            $compte->nom = htmlentities($_POST['registration_nom']);
            $compte->prenom = htmlentities($_POST['registration_prenom']);
            $compte->tel = htmlentities($_POST['registration_tel']);
            $compte->userName = $username;
            $compte->password = $hash;
            $compte->droit = $type;
            $compte->nbCo = 0;
            $compte->save();
            return true;

          }
    }catch (Exception $e) {
      return false;
    }
  }

    public static function modification($username,$password){
        try{
            $compte = Compte::where('username','=',$username)->first();
            if($compte!=null){
                return false;
            }else{

                //modification du COMPTE

                $compte = new Compte();
                $hash = password_hash($password,PASSWORD_BCRYPT);
                $type = htmlentities($_POST['registration_typecompte']);
                $compte->userName = $username;
                $compte->password = $hash;
                $compte->droit = $type;
                /**if($droit = 2){
                $compte->codePartage = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(6/strlen($x)) )),1,6);
                }*/
                $compte->save();
                return true;

            }
        }catch (Exception $e) {
            return false;
        }
    }

  public static function changementMdp($username,$password){
      try{
          $compte = Compte::where('username','=',$username)->first();
          $hash = password_hash($password,PASSWORD_BCRYPT);
          $compte->password = $hash;
          $compte->save();
          return true;
      }catch (Exception $e) {
          return false;
      }
  }

  public static function incrCo($username){
      $compte = Compte::where('username','=',$username)->first();
      $compte->nbCo++;
      $compte->save();
  }

}

 ?>
