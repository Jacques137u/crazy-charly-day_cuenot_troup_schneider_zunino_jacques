<?php


namespace projet\bdd;


use projet\modeles\tables\CreneauBDD;

class Creneau
{
    public static function modification($jour, $semaine, $h_deb, $h_fin)
    {
        try {
            $creneau = new CreneauBDD();
            $creneau->Jour = $jour;
            $creneau->Semaine = $semaine;
            $creneau->H_Deb = $h_deb;
            $creneau->H_Fin = $h_fin;
            $creneau->save();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}