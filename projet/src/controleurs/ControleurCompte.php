<?php

namespace projet\controleurs;
use projet\vues\VueCompte as VueCompte;
use projet\vues\VueCompteUtilisateur;
use projet\vues\VueHistorique as VueHistorique;

class ControleurCompte implements InterfaceControleur{

/**
  * Méthode qui affiche la vue correspondante
  * @param Request $rq : requete
  * @param Response $rs : reponse
  * @param $app : ??
  * @return Response
 **/
  public function appeler_vue($rq,$rs,$app){
    if(isset($_SESSION['userName'])) {
      $vue = new VueCompte();
      return $rs->getBody()->write($vue->afficher_vue($app,""));
    }

  }

  /**
   * Méthode qui affiche la vue correspondante
   * @param Request $rq : requete
   * @param Response $rs : reponse
   * @param $app : ??
   * @return Response
   **/
  public function afficher_vue($rq,$rs,$app){
    if(isset($_SESSION['userName'])) {
      $vue = new VueCompteUtilisateur();
      return $rs->getBody()->write($vue->afficher_vue($app,""));
    }

  }


/**
  * Méthode qui affiche l'historique
  * @param Request $rq : requete
  * @param Response $rs : reponse
  * @param $app : ??
  * @return Response
 **/
  public function afficherHistorique($rq,$rs,$app){
    if(isset($_SESSION['userName'])) {
      $vue = new VueHistorique();
      return $rs->getBody()->write($vue->afficher_vue($app,""));
    }
  }

}



 ?>
