<?php


namespace projet\controleurs;
use projet\bdd\Creneau;
use projet\vues\VueAccueil;
use projet\vues\VueCreneau;
use projet\vues\VueInscription;

class ControleurCreneau
{
    public function appeler_vue($rq,$rs,$app){
        $vue = new VueCreneau();
        return $rs->getBody()->write($vue->afficher_vue($app,""));
    }

    public function valider_creneau($rq,$rs,$app){

        $vue = new VueAccueil();
        $j = $_POST['jour'];
        $s = $_POST['semaine'];
        $hd = $_POST['h_debut'];
        $hf = $_POST['h_fin'];
        $bool = Creneau::modification($j,$s,$hd,$hf);
        return $vue->afficher_vue($app,"Votre Creneau a été crée !");
    }
}