<?php


namespace projet\controleurs;

use projet\vues\VueAfficheCreneau;
use projet\vues\VueUtilisateurs;
use projet\modeles\tables\Compte;
use projet\modeles\tables\User;


class ControleurAfficheCreneau
{
    function afficherUtilisateurs($rs, $app){
        $view = new VueAfficheCreneau();
        return $rs->getBody()->write($view->afficher_vue($app,""));
    }
}