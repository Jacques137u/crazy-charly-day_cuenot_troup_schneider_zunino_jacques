<?php


namespace projet\controleurs;


use projet\vues\VueDonnee;
use projet\vues\VueUtilisateurs;
use projet\modeles\tables\Compte;
use projet\modeles\tables\User;

class ControleurAfficherUtilisateurs
{
    function afficherUtilisateurs($rs, $app){
        $view = new VueUtilisateurs();
        return $rs->getBody()->write($view->afficher_vue($app,""));
    }

    function afficherDonnee($rs, $id, $app){
        $view = new VueDonnee();
        return $rs->getBody()->write($view->afficher_vue($app,"", $id));
    }
}