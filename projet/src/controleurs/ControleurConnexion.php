<?php

namespace projet\controleurs;

use projet\vues\VueConnexion as VueConnexion;
use projet\vues\VueAccueil as VueAccueil;
use projet\bdd\Authentification as Authentification;
use projet\vues\VuePremierCo;

class ControleurConnexion{
/**
  * Méthode qui affiche la vue correspondante
  * @param Request $rq : requete
  * @param Response $rs : reponse
  * @param $app : ??
  * @return Response
 **/
  public function appeler_vue($rq,$rs,$app){
    $vue = new VueConnexion();
    return $rs->getBody()->write($vue->afficher_vue($app,""));
  }

/**
  * Méthode qui valide la connexion et permet de se connecter
  * @param Request $rq : requete
  * @param Response $rs : reponse
  * @param $app : ??
  * @return Response
 **/
  public function valider_connexion($rq,$rs,$app){

    $message_erreur="";

    if(isset($_POST['user_name']) && isset($_POST['user_password']) && (!empty($_POST["user_name"])) &&   (!empty($_POST["user_password"]))){
      $user_nom = htmlspecialchars(filter_var($_POST['user_name'], FILTER_SANITIZE_STRING));
      $user_mdp = htmlspecialchars(filter_var($_POST['user_password'], FILTER_SANITIZE_STRING));
      if(Authentification::connexion($user_nom,$user_mdp)){
        if($_SESSION['user']['nbCo']==0 && $_SESSION['user']['droit']!=2){
          $_SESSION['user']['nbCO']+=1;
          Authentification::incrCo($user_nom);
          $vue = new VuePremierCo();
          return $rs->getBody()->write($vue->afficher_vue($app,""));
        }else{
          $_SESSION['user']['nbCo']+=1;
          Authentification::incrCo($user_nom);
          $vue = new VueAccueil();
          return $rs->getBody()->write($vue->afficher_vue($app,""));
        }

      }else{
          $message_erreur="Erreur d'identification !";
      }

    }else{
      $message_erreur="Veuillez rentrer vos identifiants !";
    }
    $vue = new VueConnexion();
    return $rs->getBody()->write($vue->afficher_vue($app,$message_erreur));
  }

/**
  * Méthode qui permet de se deconnecter
  * @param Request $rq : requete
  * @param Response $rs : reponse
  * @param $app : ??
  * @return Response
 **/
  public function deconnexion($rq,$rs,$app){
    session_destroy();
    session_start();
    $vue = new VueAccueil();
    return $rs->getBody()->write($vue->afficher_vue($app,""));
  }

  public function changer_mdp($rq, $rs, $param)
  {
    if(isset($_POST['mdp']) && !empty($_POST["mdp"])){
      $user_mdp = htmlspecialchars(filter_var($_POST['mdp'], FILTER_SANITIZE_STRING));
      if(Authentification::changementMdp($_SESSION['user']['userName'],$user_mdp)){
        $vue = new VueAccueil();
        return $rs->getBody()->write($vue->afficher_vue($param,""));
      }else{
        $erreur="Probleme de connexion";
      }
    }else{
      $erreur="Mot de passe invalide !";
    }
    $vue = new VuePremierCo();
    return $rs->getBody()->write($vue->afficher_vue($param,$erreur));
  }

}

 ?>
