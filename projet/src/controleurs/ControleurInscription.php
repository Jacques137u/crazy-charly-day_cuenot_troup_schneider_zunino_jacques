<?php

namespace projet\controleurs;
use projet\vues\VueInscription as VueInscription;
use projet\vues\VueConnexion as VueConnexion;
use projet\bdd\Authentification as Authentification;

class ControleurInscription implements InterfaceControleur{
/**
  * Méthode qui affiche la vue correspondante
  * @param Request $rq : requete
  * @param Response $rs : reponse
  * @param $app : ??
  * @return Response
 **/
  public function appeler_vue($rq,$rs,$app){
    $vue = new VueInscription();
    return $rs->getBody()->write($vue->afficher_vue($app,""));
  }

/**
  * Méthode qui permet de s'inscrire sur le site
  * @param Request $rq : requete
  * @param Response $rs : reponse
  * @param $app : ??
  * @return Response
 **/
  public function valider_inscription($rq,$rs,$app){

    $vue = new VueInscription();
    if(isset($_POST['registration_user_name'])){
      if(!empty($_POST['registration_user_name'])){
        if(!empty($_POST['registration_user_password'])&& !empty($_POST['registration_user_password_verify'])&&strlen($_POST['registration_user_password'])>5&&strlen($_POST['registration_user_password_verify'])>5){
          if($_POST['registration_user_password']===$_POST['registration_user_password_verify']){
            if($_POST['registration_typecompte']==="1"||$_POST['registration_typecompte']==="2"||$_POST['registration_typecompte']==="3"){
              $username = htmlentities($_POST['registration_user_name']);
              $password = htmlentities($_POST['registration_user_password']);
              $bool = Authentification::inscription($username,$password);
              if($bool){
                $vue = new VueInscription();
                return $vue->afficher_vue($app,"Votre compe a été crée !"); // l'utilisateur peut se connecter avec son nouveau compte
              }else{
                return $vue->afficher_vue($app,"Ce nom d'utilisateur existe déjà !");
              }
            }else{
              return $vue->afficher_vue($app,"Type de compte invalide");
            }
          }else{
            return $vue->afficher_vue($app,"Les mots de passes ne correspondent pas !");
          }
        }else{
          return $vue->afficher_vue($app,"Veuillez entrer 2 fois votre mot de passe !(6 caractères min)");
        }
      }else{
        return $vue->afficher_vue($app,"Entrer un nom d'utilisateur !");
      }

    }
  }


}


 ?>
