<?php

namespace projet\controleurs;

use projet\controleurs\InterfaceControleur as InterfaceControleur;
use projet\vues\VueAccueil as VueAccueil;

class ControleurAccueil implements InterfaceControleur{

/**
  * Méthode qui affiche la vue correspondante
  * @param Request $rq : requete
  * @param Response $rs : reponse
  * @param $app : ??
  * @return Response
 **/
  public function appeler_vue($rq,$rs,$app){
    $vue = new VueAccueil();
    return $rs->getBody()->write($vue->afficher_vue($app,""));
  }


}


 ?>
