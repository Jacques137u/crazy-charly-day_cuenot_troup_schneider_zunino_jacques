<?php

namespace projet\vues;

interface InterfaceVue{

/**
  * Méthode qui permet de générer l'entete des pages
  * @param ?? $app
  * @return String $html
 **/
  public static function generer_Entete($app);

/**
  * Méthode qui permet de générer le footer des pages
  * @param ?? $app
  * @return String $html
 **/
  public static function generer_basdepage($app);

/**
  * Méthode qui permet de générer le corps de la page correspondante
  * @param ?? $app
  * @param $erreur
  * @return String $html
 **/
  public function afficher_vue($app,$erreur);
}

 ?>
