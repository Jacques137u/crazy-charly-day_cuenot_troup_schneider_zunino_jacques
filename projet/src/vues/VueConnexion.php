<?php

namespace projet\vues;

class VueConnexion implements InterfaceVue{

/**
  * Méthode qui permet de générer l'entete des pages
  * @param ?? $app
  * @return String $html
 **/
  public static function generer_Entete($app){
    $path = $app->router->pathFor('route_index');
    $html= <<<END
    <html>
    	<head>
    		<title> Crazy Charly Day </title>
    		<meta  charset="utf-8">
    		<link href="$path/css/style.css" rel="stylesheet" type="text/css">
			<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    	</head>
      <body>
END;
    return $html;
  }

/**
  * Méthode qui permet de générer le footer des pages
  * @param ?? $app
  * @return String $html
 **/
  public static function generer_basdepage($app){
    $html = <<<END
    <footer>
        <p><a href="">Nous contacter</a> - <a href=""> A propos </a></p>
    </footer>
      </body>
    </html>
END;
return $html;
  }

/**
  * Méthode qui permet de générer le corps de la page correspondante
  * @param ?? $app
  * @param $erreur
  * @return String $html
 **/
  public function afficher_vue($app,$erreur){

    //Generation des liens nécessaires sur la page
      $path = $app->router->pathFor('route_index');
      $url2 =  $app->router->pathFor('route_connexion');
      $url3 =  $app->router->pathFor('route_deconnexion');
      $url4 =  $app->router->pathFor('route_modification');
      $url5 = $app->router->pathFor('route_inscription');
      $url6 = $app->router->pathFor('route_liste0');
      $url7 = $app->router->pathFor('NV_Creneau');
    //-
    //Generation de l'entête
    $html = self::generer_Entete($app);
    //-
    //Ecriture du code html propre à la page --------------------------
      $bool_connecte = false;
      if(isset($_SESSION['user'])){
          if(!empty($_SESSION['user'])){
              $bool_connecte = true;
          }
      }
      if($bool_connecte){
          if($_SESSION['user']['droit']==2){
              $username = $_SESSION['user']['userName'];
              $html = $html . <<<END
      <header>
        <nav>
          <ul class="navcompte">
              <li> $username </li>
              <li><a href </li>
                <li><a href="$url4"> Modification</a></li>
                <li><a href="$url3">Deconnexion</a></li>
                <li><a href ="$url7">Creneau</a></li>
            <li><a href="$url5">Creer un compte</a></li>
            <li><a href="$url6">Liste utilisateur</a></li>
          </ul>
        </nav>
      </header>
        <nav>
          <ul class="navbar">
            <li><a href="$path">connexion</a></li>
          </ul>
        </nav>
END;
          }else{
              $username = $_SESSION['user']['userName'];
              $html = $html . <<<END
              <header>
                <nav>
                  <ul class="navcompte">
                  <li><a href="$url4"> Modification</a></li>
                  <li><a href="$url3">Deconnexion</a></li>
                      <li> $username  </li>
                    <li><a href="$url6">Liste utilisateur</a></li>

                  </ul>
                </nav>
              </header>
                <nav>
                  <ul class="navbar">
                    <li><a href="$path">connexion</a></li>
                  </ul>
                </nav>
END;
          }
      }else{
          $html = $html . <<<END
      <header>

        <nav>
          <ul class="navcompte">
              <li><a href="$url2">Connexion</a></li>
          </ul>
        </nav>
      </header>
        <nav>
          <ul class="navbar">
            <li><a href="$path">Connexion</a></li>
          </ul>
        </nav>
END;
      }

      //-
      //Ecriture du code html propre à la page -------------------------------
      $html = $html . <<<END


    <section>
      <div id="contenu">

      <h2> Connexion </h2>
      <h3> $erreur </h3>
        <form action="$url2" method="post">
          <div class="form_elem">
            <label for="name"> Nom d'utilisateur </label>
            <input type="text" id="name" name="user_name">
          </div>
          <div class="form_elem">
            <label for="password">Mot de passe</label>
            <input type="password" id="password" name="user_password"
          </div>
          <div class="form_elem">
          <input id="button" type="submit" value="Connexion">
          </div>
        </form>
        <p> Pas de compte ? Inscrivez-vous <a href="$url5" > ici </a> !
      </div>
    </section>

END;
    //----------------------------
    //Generation du bas de page
    $html = $html . self::generer_basdepage($app);
    //-

    return $html;
  }


}

 ?>
