<?php

namespace projet\modeles\tables ;
use Illuminate\Database\Eloquent\Model as Model;

class Compte extends Model{

    protected $table = 'compte';
    protected $primaryKey = 'id' ;
    public $timestamps = false ;
}
