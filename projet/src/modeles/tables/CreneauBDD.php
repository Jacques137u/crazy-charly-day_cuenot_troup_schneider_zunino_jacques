<?php


namespace projet\modeles\tables;


use Illuminate\Database\Eloquent\Model;

class CreneauBDD extends Model
{
    protected $table = 'creneau';
    protected $primaryKey = 'ID' ;
    public $timestamps = false ;
}