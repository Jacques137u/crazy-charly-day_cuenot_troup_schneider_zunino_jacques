<?php

require_once __DIR__ . '/src/vendor/autoload.php';
use Illuminate\Database\Capsule\Manager as DB;
use projet\controleurs\ControleurAccueil;
use projet\controleurs\ControleurConnexion;
use projet\controleurs\ControleurInscription;
use projet\controleurs\ControleurAfficherUtilisateurs;
use projet\controleurs\ControleurModification;

//Creation Slim
$conf =[
        'settings'=>[
          'displayErrorDetails' => true,
        ],
      ];

$app = new \Slim\App($conf);
//-

$db = new DB();
$db->addConnection(parse_ini_file("src/conf/conf.ini"));

$db->setAsGlobal();
$db->bootEloquent();

//-

//Simplification des appels de classe


//-

// GET/POST



$app->get('/',function($rq,$rs){
    $c = new ControleurConnexion();
    return $c->appeler_vue($rq,$rs,$this);
})->setName('route_index');


$app->get('/Connexion',function($rq,$rs){
    $c = new ControleurConnexion();
    return $c->appeler_vue($rq,$rs,$this);
})->setName('route_connexion');

$app->post('/Connexion',function($rq,$rs){
    $c = new ControleurConnexion();
    return $c->valider_connexion($rq,$rs,$this);
})->setName('route_connexion');

$app->get('/Modification',function($rq,$rs){
    $c = new ControleurModification();
    return $c->appeler_vue($rq,$rs,$this);
})->setName('route_modification');


$app->post('/Modification',function($rq,$rs){
    $c = new ControleurModification();
    return $c->valider_modification($rq,$rs,$this);
})->setName('route_modification');


$app->get('/Deconnexion', function($rq,$rs){
  $c = new  ControleurConnexion();
  return $c->deconnexion($rq,$rs,$this);
})->setName("route_deconnexion");

$app->get('/Inscription', function($rq,$rs){
  $c = new  ControleurInscription();
  return $c->appeler_vue($rq,$rs,$this);
})->setName("route_inscription");

$app->post('/Inscription', function($rq,$rs){
  $c = new  ControleurInscription();
  return $c->valider_inscription($rq,$rs,$this);
})->setName("route_inscription");

$app->get('/ListeUtilisateur', function($rq,$rs, $no){
    $c = new ControleurAfficherUtilisateurs();
    return $c->afficherUtilisateurs($rs, $this);
})->setName("route_liste0");;;

$app->post('/ChangementMdp',function($rq,$rs){
    $c = new  ControleurConnexion();
    return $c->changer_mdp($rq,$rs,$this);
})->setName("route_changementMdp");

$app->get('/creneau', function($rq,$rs){
    $c = new  \projet\controleurs\ControleurCreneau();
    return $c->appeler_vue($rq,$rs,$this);
})->setName("NV_Creneau");


$app->post('/creneau', function($rq,$rs){
    $c = new  \projet\controleurs\ControleurCreneau();
    return $c->valider_creneau($rq,$rs,$this);
})->setName("NV_Creneau");


$app->get('/ListeUtilisateur/{id}', function($rq,$rs, array $args){
    $id = $args['id'];
    $c = new ControleurAfficherUtilisateurs();
    return $c->afficherDonnee($rs,$id,$this);
})->setName("route_liste");;

$app->get('/ListeCreneau', function($rq,$rs, array $args){
    $id = $args['id'];
    $c = new ControleurAfficherUtilisateurs();
    return $c->afficherDonnee($rs,$id,$this);
})->setName("route_liste_creneau");;

session_start();

$app->run();


 ?>
