-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  jeu. 13 fév. 2020 à 14:41
-- Version du serveur :  5.5.64-MariaDB
-- Version de PHP :  7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `cuenot7u`
--

-- --------------------------------------------------------

--
-- Structure de la table `compte`
--

CREATE TABLE `compte` (
  `id` int(5) NOT NULL,
  `userName` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `droit` int(5) NOT NULL DEFAULT '1',
  `nbCo` int(5) DEFAULT NULL,
  `mail` varchar(250) DEFAULT NULL,
  `nom` varchar(250) DEFAULT NULL,
  `prenom` varchar(250) DEFAULT NULL,
  `tel` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `compte`
--

INSERT INTO `compte` (`id`, `userName`, `password`, `droit`, `nbCo`, `mail`, `nom`, `prenom`, `tel`) VALUES
(9, 'root', '$2y$10$mDSJLMD5HGaMHZ/RSv.88OQ5GO5B0aPD4t57hEgOiiE2vWe2PA2Sa', 1, NULL, NULL, NULL, NULL, NULL),
(10, 'tom.mendez@gmail.com', '$2y$10$Odb/1q/4lt0gThIGO5wEq.9pPcdOD0ZmHN0gcdNhogBKlpInFTSkC', 1, NULL, NULL, NULL, NULL, NULL),
(11, 'test', '$2y$10$xyMWqZyFnhwC1viT3hPof.e6cpQLruWSPduQ1JTS.vSok4Smylli6', 1, 0, NULL, NULL, NULL, NULL),
(12, 'test2', '$2y$10$d5xDYzQ3hp1hHGMwEk6.fO0Y1qtQiiYyCvY1qungZkRiNmm8tDsBy', 2, 3, NULL, NULL, NULL, NULL),
(13, 'tt', '$2y$10$6K.MsRJ4Cgm.WDKHWxN0c.2gSEQ0to/Ov.2YjaqS2HR0BwB4.mFqW', 1, 1, NULL, NULL, NULL, NULL),
(14, 'tttt', '$2y$10$9.geKMeNIJxoHQAo.zG2JuPy9/5e1w.aLII0jmPT13OzPdzmVSgcy', 2, 0, 'test@gmail.com', 'sacha', 'cuenot', 6585515);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `compte`
--
ALTER TABLE `compte`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `compte`
--
ALTER TABLE `compte`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
